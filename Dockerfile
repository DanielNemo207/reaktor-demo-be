FROM ubuntu:18.04
MAINTAINER Duc VU "duc.vu.working@gmail.com"

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev

RUN apt-get install vim -y

RUN pip3 install pandas && \
    pip3 install numpy

COPY ./requirements.txt /requirements.txt

WORKDIR /

RUN pip3 install -r requirements.txt

COPY . /

#ENTRYPOINT [ "python3", "-m" ]

CMD [ "python3", "-m", "src.main" ]