from src.setting import config as cf
import json

class Package(object):

    def __init__(self, name, description, version, dependencies):
        self.name = name
        self.description = description
        self.version = version
        self.dependencies = dependencies
        # add more later

        self.rev_dependencies = []

    def to_json_format(self):
        return cf.json_temp.substitute(
            name=self.name,
            version=self.version,
            description=json.dumps(self.description),
            depends=json.dumps(self.dependencies),
            rev_depends=json.dumps(self.rev_dependencies),
            )