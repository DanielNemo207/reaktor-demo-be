import re

from src.utils.package import Package
from src.setting import config as cf

package_seperator = '\n\n'

def remove_last_line_from_string(s):
    return s[:s.rfind('\n')]

def read_file():
    '''
    Read file by file path in config file
    Extract information(name, version, description, dependencies) of packages from the file content
    Enrich Rev-dependencies information for packages
    Return list of enriched packages
    '''
    packages = []

    file_content = load_file_from_path()
    raw_package_contents = file_content.split(package_seperator)

    for raw_content in raw_package_contents:
        name = extract_package_name(raw_content)
        version = extract_package_version(raw_content)
        description = extract_package_description(raw_content)
        depends = extract_package_depends(raw_content)

        package = Package(name, description, version, depends)
        packages.append(package)
    
    packages = enrich_rev_depends_info(packages)
    return sorted(packages, key=lambda x: x.name)

def load_file_from_path():
    file_content = open(cf.file_path, encoding="utf-8").read()
    return file_content
    
def search_package_info_by_pattern(raw_content, pattern):
    extractor = re.compile(pattern)
    matches = extractor.search(raw_content)

    if matches is not None:
        return matches.group(0)
    return ''

def extract_package_name(raw_content):
    name_match = search_package_info_by_pattern(raw_content, cf.pack_name_pattern)
    name = re.sub(cf.pack_name_sub, '', name_match)[:-1]
    if name == '':
        return 'no-name'
    return name.strip()

def extract_package_version(raw_content):
    version_match = search_package_info_by_pattern(raw_content, cf.pack_version_pattern)
    version = re.sub(cf.pack_version_sub, '', version_match)[:-1]
    return version.strip()

def extract_package_depends(raw_content):
    depends_match = search_package_info_by_pattern(raw_content, cf.pack_depend_pattern)

    depends_str = remove_last_line_from_string(depends_match)
    depends_str = re.sub(cf.pack_depends_sub, '', depends_str)

    depends = depends_str.split(',')
    depends_without_ver = [re.sub(r'\(.+\)', '', depend).strip() for depend in depends]

    return list(set(depends_without_ver))

def extract_package_description(raw_content):
    desc_match = search_package_info_by_pattern(raw_content, cf.pack_desc_pattern)
    desc = re.sub(cf.pack_desc_sub, '', desc_match)
    desc = remove_last_line_from_string(desc)
    return desc.strip()

def enrich_rev_depends_info(packages):
    for package_m in packages:
        for package_n in packages:
            if package_m.name == package_n.name:
                continue

            if package_m.name.strip() in package_n.dependencies:
                package_m.rev_dependencies.append(package_n.name.strip())

    return packages