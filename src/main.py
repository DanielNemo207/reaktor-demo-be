from flask import Flask, request
from flask import jsonify
import os

from src.utils.read_file import read_file

app = Flask(__name__)
port = int(os.environ.get("PORT", 5000))
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

@app.route('/', methods=['GET'])
def hello_world():
    return 'Reaktor pre-assignment'

@app.route('/packages', methods=['GET'])
def get_packages():
    packages = read_file()
    
    packages_response = []
    for package in packages:
        packages_response.append(package.to_json_format())

    response = {'packages': packages_response, 'mess': 'OK'}

    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0',port=port)