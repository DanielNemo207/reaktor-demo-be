from string import Template

# the path of file in Ubuntu
file_path = '/var/lib/dpkg/status'

# Package information regex pattern
# one line features
pack_name_sub = r'Package:'
pack_name_pattern = r'Package: .+\n'

pack_version_sub = r'Version:'
pack_version_pattern = r'Version: .+\n'

# multiple lines features
# r'Description: [\s\S]+:'
pack_desc_sub = r'Description:'
pack_desc_pattern = r'Description: [^:]+'

pack_depends_sub = r'(Pre-Depends|Depends):'
pack_depend_pattern = r'(Pre-Depends|Depends): [^:]+'


# json string format template
json_temp = Template('{\
                        "Package": "${name}",\
                        "Version": "${version}",\
                        "Dependencies": ${depends},\
                        "Rev-dependencies" : ${rev_depends},\
                        "Description": ${description}\
                    }')
