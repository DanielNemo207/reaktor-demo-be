import unittest
from unittest.mock import patch
from parameterized import parameterized

import unittests.mock_data as mock_data
import src.utils.read_file as read_file 

class Utils_Tests(unittest.TestCase):
    '''
    UTs for src.utils.read_file
    '''

    @parameterized.expand([
        [mock_data.search_by_pattern_correct, mock_data.isNone_expect_for_correct_search_pattern], 
        [mock_data.search_by_pattern_wrong, mock_data.isNone_expect_for_wrong_search_pattern]
    ])
    def test_search_package_info_by_pattern(self, pattern, expectation):
        '''
        UT for function read_file.search_package_info_by_pattern
        Expecting to find search object with correct regex pattern
        '''
        search_match = read_file.search_package_info_by_pattern(mock_data.mock_string_for_search_by_pattern, pattern)
        self.assertEqual(search_match == '', expectation)

    def test_extract_package_name(self):
        '''
        UT for function read_file.extract_package_name
        Expecting to find expected name given mock input
        '''
        name = read_file.extract_package_name(mock_data.mock_package_1)
        self.assertEqual(name, mock_data.expected_name_1)

    def test_extract_package_version(self):
        '''
        UT for function read_file.extract_package_version
        Expecting to find expected version given mock input
        '''
        version = read_file.extract_package_version(mock_data.mock_package_1)
        self.assertEqual(version, mock_data.expected_version_1)

    def test_extract_package_description(self):
        '''
        UT for function read_file.extract_package_description
        Expecting to find expected description given mock input
        '''
        des = read_file.extract_package_description(mock_data.mock_package_1)
        self.assertEqual(des, mock_data.expected_description_1)

    def test_extract_package_depends(self):
        '''
        UT for function read_file.extract_package_depends
        Expecting to find expected dependencies given mock input
        '''
        depends = read_file.extract_package_depends(mock_data.mock_package_1)
        self.assertEqual(sorted(depends), sorted(mock_data.expected_depends_1))

    def test_enrich_rev_depends_info(self):
        '''
        UT for function read_file.enrich_rev_depends_info
        Expecting to enrich correctely as expected rev-dependencies given mock input
        '''
        enriched_packages = read_file.enrich_rev_depends_info(mock_data.mock_packages)
        self.assertEqual(enriched_packages[0].rev_dependencies, mock_data.expected_rev_depends_1)
        self.assertEqual(enriched_packages[1].rev_dependencies, mock_data.expected_rev_depends_2)

    @patch('src.utils.read_file.load_file_from_path', return_value=mock_data.mock_content)
    @patch('src.utils.read_file.extract_package_name', return_value=mock_data.expected_name_1)
    @patch('src.utils.read_file.extract_package_version', return_value=mock_data.expected_description_1)
    @patch('src.utils.read_file.extract_package_description', return_value=mock_data.expected_description_1)
    @patch('src.utils.read_file.extract_package_depends', return_value=mock_data.expected_depends_1)
    @patch('src.utils.read_file.enrich_rev_depends_info', return_value=mock_data.mock_packages)
    def test_read_file(self, mock_load, mock_name_extract, mock_ver_extract, 
                        mock_des_extract, mock_depends_extract, mock_rev_depends_enrich):

        '''
        UT for read_file.read_file function

        Mock input has 2 elements

        Expecting to call read_file.load_file_from_path() once
        Expecting to call read_file.extract_package_name() twice
        Expecting to call read_file.extract_package_version() twice
        Expecting to call read_file.extract_package_description() twice
        Expecting to call read_file.extract_package_depends() twice
        Expecting to call read_file.enrich_rev_depends_info() once
        '''
            
        packages = read_file.read_file()

        assert mock_load.called
        assert mock_load.call_count == mock_data.expected_call_count_of_file_load

        assert mock_name_extract.called
        assert mock_name_extract.call_count == mock_data.expected_call_count_of_extractor

        assert mock_ver_extract.called
        assert mock_ver_extract.call_count == mock_data.expected_call_count_of_extractor

        assert mock_des_extract.called
        assert mock_des_extract.call_count == mock_data.expected_call_count_of_extractor

        assert mock_depends_extract.called
        assert mock_depends_extract.call_count == mock_data.expected_call_count_of_extractor

        assert mock_rev_depends_enrich.called
        assert mock_rev_depends_enrich.call_count == mock_data.expected_call_count_of_rev_depends_enrich

        self.assertEqual(len(packages), mock_data.expected_call_count_of_extractor)

if __name__ == '__main__':
    unittest.main()