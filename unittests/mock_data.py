import pandas as pd
from src.utils.package import Package

# mock data for testing function search by pattern
mock_string_for_search_by_pattern = 'search_object test \n'
search_by_pattern_correct = r'^search_object .+\n'
isNone_expect_for_correct_search_pattern = False

search_by_pattern_wrong = r'^None exist object .+\n'
isNone_expect_for_wrong_search_pattern= True

# mock data for package info extractor
# mock package 1
mock_package_1 = '''Package: python-pkg-resources
Status: install ok installed
Priority: optional
Section: python
Installed-Size: 175
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Architecture: all
Source: distribute
Version: 0.6.24-1ubuntu1
Replaces: python2.3-setuptools, python2.4-setuptools
Provides: python2.6-setuptools, python2.7-setuptools
Depends: python (>= 2.6), python (<< 2.8)
Suggests: python-distribute, python-distribute-doc
Conflicts: python-setuptools (<< 0.6c8-3), python2.3-setuptools (<< 0.6b2), python2.4-setuptools (<< 0.6b2)
Description: Package Discovery and Resource Access using pkg_resources
 The pkg_resources module provides an API for Python libraries to
 access their resource files, and for extensible applications and
 frameworks to automatically discover plugins.  It also provides
 runtime support for using C extensions that are inside zipfile-format
 eggs, support for merging packages that have separately-distributed
 modules or subpackages, and APIs for managing Python's current
 "working set" of active packages.
Original-Maintainer: Matthias Klose <doko@debian.org>
Homepage: http://packages.python.org/distribute
Python-Version: 2.6, 2.7'''

expected_name_1 = 'python-pkg-resources'
expected_description_1 = '''Package Discovery and Resource Access using pkg_resources
 The pkg_resources module provides an API for Python libraries to
 access their resource files, and for extensible applications and
 frameworks to automatically discover plugins.  It also provides
 runtime support for using C extensions that are inside zipfile-format
 eggs, support for merging packages that have separately-distributed
 modules or subpackages, and APIs for managing Python's current
 "working set" of active packages.'''
expected_version_1 = '0.6.24-1ubuntu1'
expected_depends_1 = ['python']
expected_rev_depends_1 = ['libws-commons-util-java']


# mock package 2
mock_package_2 = '''Package: libws-commons-util-java
Status: install ok installed
Priority: optional
Section: java
Installed-Size: 101
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Architecture: all
Depends: python-pkg-resources
Version: 1.0.1-7
Description: Common utilities from the Apache Web Services Project
 This is a small collection of utility classes, that allow high
 performance XML processing based on SAX.
Original-Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Homepage: http://ws.apache.org/commons/util/'''

expected_name_2 = 'libws-commons-util-java'
expected_description_2 = '''Common utilities from the Apache Web Services Project
 This is a small collection of utility classes, that allow high
 performance XML processing based on SAX.'''
expected_version_2 = '1.0.1-7'
expected_depends_2 = ['python-pkg-resources']
expected_rev_depends_2 = []

# mock packages
mock_package_obj_1 = Package(expected_name_1, expected_description_1, expected_version_1, expected_depends_1)
mock_package_obj_2 = Package(expected_name_2, expected_description_2, expected_version_2, expected_depends_2)
mock_packages = [mock_package_obj_1, mock_package_obj_2]

# mock data for test read_file
mock_content = mock_package_1 + '\n\n' + mock_package_2

expected_call_count_of_extractor = 2
expected_call_count_of_file_load = 1
expected_call_count_of_rev_depends_enrich = 1
