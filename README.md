# Reaktor assignment #
## Assignment: https://www.reaktor.com/junior-dev-assignment/ ##

1. **Apps**
	-root (root directory of app)
      |-src (main app)
         |- setting
            |- config.py
         |- utils
            |- package.py
            |- read_file.py
         |- main.py (main)
      |-unittests
         |- mock_data.py (mock)
         |- utils_ut.py (ut)
      README.md

2. **Deployment**
    - Heroku url: https://reaktor-demo-be.herokuapp.com/
    - Deployed by Docker + Heroku container